#!/usr/bin/make -f

include /usr/share/dpkg/architecture.mk
include /usr/share/dpkg/vendor.mk
export CROSS=$(DEB_HOST_GNU_TYPE)-
export DEB_BUILD_MAINT_OPTIONS=hardening=+all
export CFLAGS:=$(shell dpkg-buildflags --get CFLAGS) $(shell dpkg-buildflags --get CPPFLAGS)
export DESTDIR=$(CURDIR)/debian/linuxlogo
export PREFIX=/usr

%:
	dh $@

logo_config:
	find ./logos -type f | LC_ALL=C sort > $@

# Note: NO DEPENDENCY on po/linux_logo.pot. This stanza creates a
# backup, because the original gets modified during the build process,
# so it needs to be restored while cleaning post-build. (The file
# is automatically generated, so it would be even better to just not
# have it in the upstream tarball.)
po/linux_logo.pot.BAK:
	[ ! -f $@ ] && cp --verbose po/linux_logo.pot $@

override_dh_auto_configure: logo_config po/linux_logo.pot.BAK
	./configure

debian/local/linux_logo.conf:
	d=$(dir $@);						\
	for v in $(DEB_VENDOR) $(DEB_PARENT_VENDOR) Debian; do	\
	  if [ -r $${d}linux_logo.$${v}.conf ]; then		\
	    cp -p --verbose $${d}linux_logo.$${v}.conf $@;	\
	    break;						\
	  fi							\
	done

execute_before_dh_install: debian/local/linux_logo.conf

execute_after_dh_install: debian/linuxlogo/usr/share/man/man1/linux_logo.1

debian/linuxlogo/usr/share/man/man1/linux_logo.1: linux_logo.1.gz
	mkdir -p $(dir $@)
	zcat $< > $@
	sed --in-place s:infomation:information: $@
	sed --in-place s:/usr/doc/linux_logo:/usr/share/doc/linuxlogo/: $@

override_dh_auto_clean:
	[ ! -f po/linux_logo.pot.BAK ] || cp --verbose po/linux_logo.pot.BAK po/linux_logo.pot
	-cmp --quiet po/linux_logo.pot.BAK po/linux_logo.pot && rm --verbose -f po/linux_logo.pot.BAK
	[ ! -f Makefile ] || ./configure && make distclean
